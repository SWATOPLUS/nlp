import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {DictionaryComponent} from './dictionary/dictionary.component';
import {DownloadFileComponent} from './download-file/download-file.component';
import {FileReaderComponent} from './file-reader/file-reader.component';
import {FileLoaderComponent} from './file-reader/file-loader/file-loader.component';


@NgModule({
  declarations: [
    AppComponent,
    DictionaryComponent,
    DownloadFileComponent,
    FileReaderComponent,
    FileLoaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
