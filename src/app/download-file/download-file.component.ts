import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';

export class ParsedText {
  wordArray: string[];
  text: string;

  constructor(wordArray: string[], text: string) {
    this.wordArray = wordArray;
    this.text = text;
  }
}

@Component({
  selector: 'app-download-file',
  templateUrl: './download-file.component.html',
  styleUrls: ['./download-file.component.css']
})
export class DownloadFileComponent {

  @Output()
  fileParsed: EventEmitter<ParsedText> = new EventEmitter();

  @ViewChild('fileInput')
  dialogWindow: ElementRef;
  files: File[] = [];

  openChooseFileDialog(event) {
    Array.from(event.srcElement.files).forEach((file: File) => {
      this.files.push(file);
    });
  }

  openFileDialog() {
    this.dialogWindow.nativeElement.click();
  }

  onFileLoaded(text: string) {
    this.parseText(text);
  }

  parseText(text: string) {
    const array: string[] = Array.from(text.match(new RegExp('[a-z]+-[a-z]+|[a-z]+', 'gi'))).map((word) => {
      return word.toLowerCase();
    });
    this.fileParsed.next(new ParsedText(array, text));
  }
}
