import {Component, ElementRef, ViewChild} from '@angular/core';
import {ParsedText} from '../download-file/download-file.component';
import {saveAs} from 'file-saver/dist/FileSaver';
import posTagger from 'wink-pos-tagger';
import {Tag} from 'en-pos';
import {lexicon} from 'en-lexicon';
// import WordPOS from 'wordpos';
// import tagger from 'en-pos';


const enum sortType {
  SORT_TYPE_WORD,
  SORT_TYPE_COUNT
}

interface TaggedWord {
  value: string;
  tag: string;
  normal: string;
  pos: string;
  lemma?: string;
}

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.css']
})

export class DictionaryComponent {
  wordMap: Map<string, { count: number, tags: string }> = new Map();
  wordArray = [];
  textArray: string[] = [];
  textMap: Map<string, number[]> = new Map();

  wordUp = false;
  countUp = false;
  editWord = '';
  newWord = '';
  private last: sortType;
  private tagger;
  private wordPOS;

  @ViewChild('dialog')
  dialogWindow: ElementRef;

  constructor() {
    this.tagger = posTagger();
    const tags = new Tag(['work', 'works'])
      .initial() // initial dictionary and pattern based tagging
      .smooth()
      .tags;
    console.log(tags);
    const tags2 = new Tag(['work', 'works'])
      .initial() // initial dictionary and pattern based tagging
      .tags;
    console.log(tags2);
    console.log(lexicon.work);
    // this.wordPOS = WordPOS();
    // var wordpos = new WordPOS();
  }

  onFileParsed(parsedText: ParsedText) {
    console.log('onFileParsed = ');
    console.log(parsedText.wordArray);
    this.textArray.push(parsedText.text);
    const taggedArray = this.tagger.tagSentence(parsedText.text);
    console.log('TAGGER', taggedArray);
    parsedText.wordArray.forEach((word: string) => {
      if (this.wordMap.has(word)) {
        this.wordMap.set(word, {
          count: this.wordMap.get(word).count + 1,
          tags: this.wordMap.get(word).tags
        });
        this.textMap.get(word).push(this.textArray.length - 1);
      } else {
        this.wordMap.set(word, {count: 1, tags: this.getTagArray(word).join(', ')});
        this.textMap.set(word, [this.textArray.length - 1]);
      }
    });
    this.wordArray = Array.from(this.wordMap);
    this.last = sortType.SORT_TYPE_WORD;
    this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
  }

  wordSortClick(event): void {
    console.log('WordSort click');
    this.wordUp = !this.wordUp;
    if (this.last === sortType.SORT_TYPE_WORD) {
      this.wordArray = this.wordArray.reverse();
    } else {
      this.last = sortType.SORT_TYPE_WORD;
      this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
    }
    console.log('FIRST = ' + this.wordArray[0]);
  }

  countSortClick(event): void {
    console.log('CountSort click');
    this.countUp = !this.countUp;
    if (this.last === sortType.SORT_TYPE_COUNT) {
      this.wordArray = this.wordArray.reverse();
    } else {
      this.last = sortType.SORT_TYPE_COUNT;
      this.wordArray = this.countUp ? this.wordArray.sort(compareCountDown) : this.wordArray.sort(compareCountUp);
    }
    console.log('FIRST = ' + this.wordArray[0]);
  }

  editItem(word): void {
    console.log('EDIT ITEM ', word);
    this.newWord = word;
    this.editWord = word;
    this.dialogWindow.nativeElement.showModal();
  }

  saveWord(): void {
    if (this.newWord === this.editWord) {
      return;
    }
    if (this.wordMap.has(this.newWord)) {
      this.wordMap.set(this.newWord, {
        count: this.wordMap.get(this.newWord).count + this.wordMap.get(this.editWord).count,
        tags: this.wordMap.get(this.newWord).tags
      });
      const textNumberArray: number[] = this.textMap.get(this.newWord);
      this.textMap.get(this.newWord).concat(this.textMap.get(this.editWord).filter(oldValueTextNumber => {
        return !textNumberArray.includes(oldValueTextNumber);
      }));
      this.resetAfterWordChange(this.editWord, this.newWord);
    } else {
      this.wordMap.set(this.newWord, {
        count: this.wordMap.get(this.editWord).count,
        tags: this.getTagArray(this.newWord).join(', ')
      });
      this.textMap.set(this.newWord, this.textMap.get(this.editWord));
      this.resetAfterWordChange(this.editWord, this.newWord);
      this.last = sortType.SORT_TYPE_WORD;
      this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
    }
    this.editWord = '';
    this.newWord = '';
    this.dialogWindow.nativeElement.close();
  }

  private resetAfterWordChange(editWord: string, newWord: string) {
    const self = this;
    this.textMap.get(editWord).forEach(textNumber => {
      self.textArray[textNumber] = self.textArray[textNumber].replace(editWord, newWord);
    });
    this.wordMap.delete(editWord);
    this.textMap.delete(editWord);
    this.wordArray = Array.from(this.wordMap);
  }

  saveEtalonText(event) {
    const text = this.textArray.join(' ');
    console.log(text);
    const blob = new Blob([text], {type: 'text/plain'});
    saveAs(blob, 'etalonText.txt');
  }

  StemWords(event) {
    const tagger = posTagger();
    console.log('TAGGER', tagger.tagSentence('He is trying to fish for fish in the lake.'));
  }

  getTagArray(word: string): string [] {
    const tags: string[] = lexicon[word.toLowerCase()] ? lexicon[word.toLowerCase()].split('|') : [];
    return tags;
  }
}

function compareWordUp(a, b) {
  if (a[0] < b[0]) {
    return -1;
  }
  if (a[0] > b[0]) {
    return 1;
  }
  return 0;
}

function compareWordDown(a, b) {
  if (a[0] > b[0]) {
    return -1;
  }
  if (a[0] < b[0]) {
    return 1;
  }
  return 0;
}

function compareCountUp(a, b) {
  if (a[1].count < b[1].count) {
    return -1;
  }
  if (a[1].count > b[1].count) {
    return 1;
  }
  return 0;
}

function compareCountDown(a, b) {
  if (a[1].count < b[1].count) {
    return -1;
  }
  if (a[1].count > b[1].count) {
    return 1;
  }
  return 0;
}
