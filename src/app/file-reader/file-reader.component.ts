import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-file-reader',
  templateUrl: './file-reader.component.html',
  styleUrls: ['./file-reader.component.css']
})
export class FileReaderComponent {
  @Input()
  files: File[] = [];

  @Output()
  fileLoaded: EventEmitter<any> = new EventEmitter();

  onFileLoaded(event) {
    this.fileLoaded.emit(event.result);
  }
}
